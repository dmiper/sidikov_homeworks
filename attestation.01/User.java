package attestation_01;

import java.util.Objects;

public class User {
    private final int ID;
    private String name;
    private int age;
    private final boolean works;

    public User(int id, String name, int age, boolean works) {
        this.ID = id;
        this.name = name;
        this.age = age;
        this.works = works;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorks() {
        return works;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return ID == user.ID && age == user.age && works == user.works && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, name, age, works);
    }

    @Override
    public String toString() {
        return "Пользователь: " +
                "ID = " + ID +
                ", Имя = " + name +
                ", Возраст = " + age +
                ", Работает = " + works;
    }
}
