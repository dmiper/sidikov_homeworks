package attestation_01.repository;

import attestation_01.User;

import java.util.Map;

public interface FileService {
    Map<Integer, User> inputFile(String fileName);
    void updateUser(Map<Integer, User> users, String fileName);
}
