package attestation_01.repository;

import attestation_01.User;

public interface UserRepositoryFile {
    User findByID(int ID) throws NullPointerException;
    void update(User user);
}
