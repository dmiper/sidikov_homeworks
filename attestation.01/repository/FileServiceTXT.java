package attestation_01.repository;

import attestation_01.User;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FileServiceTXT implements FileService {
    @Override
    public Map<Integer, User> inputFile(String fileName) {
        Map<Integer, User> usersFileTXT = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            reader.lines().map(sting -> sting.split("\\|")).forEach(part -> {
                int ID = Integer.parseInt(part[0]);
                String name = part[1];
                int age = Integer.parseInt(part[2]);
                boolean works = Boolean.parseBoolean(part[3]);
                usersFileTXT.put(ID, new User(ID, name, age, works));
            });
        } catch (IOException ignore) {
        }
        return usersFileTXT;
    }

    @Override
    public void updateUser(Map<Integer, User> users, String fileName) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))){
            Set<Map.Entry<Integer, User>> entries = users.entrySet();
            for (Map.Entry<Integer, User> entry : entries)
                writer.write(entry.getValue().getID() + "|" +
                        entry.getValue().getName() + "|" +
                        entry.getValue().getAge() + "|" +
                        entry.getValue().isWorks() + "\n"
                );
        } catch (IOException ignore) {
        }
    }
}
