package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.services.ProductsService;

import java.util.List;

@Controller
public class ProductController {
    private final ProductsService productService;

    @Autowired
    public ProductController(ProductsService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public String getProductPage(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer productId) {
        Product product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @PostMapping("/products")
    public String addProduct(ProductForm form){
        productService.addProduct(form);
        return "redirect:/product";
    }

    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productService.deleteProduct(productId);
        return "redirect:/product";
    }

    @PostMapping("/products/{product-id}/update")
    public String updateProduct(ProductForm form, @PathVariable("product-id") Integer productId) {
        productService.updateProduct(form, productId);
        return "redirect:/product";
    }
}
