package ru.pcs.web.forms;

import lombok.Data;

@Data
public class ProductForm {
    private String description;
    private Integer cost_rub;
    private Integer quantity;
}
