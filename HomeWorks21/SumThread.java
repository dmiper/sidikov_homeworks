package hw21;

public class SumThread extends Thread{
    private final int from;
    private final int to;
    private final int number;
    private final int[] array = Main.array;
    private final int[] threadSums = Main.threadSums;

    public SumThread(int from, int to, int number) {
        this.from = from;
        this.to = to;
        this.number = number;
    }

    @Override
    public void run() {
        int threadSum = 0;
        for (int i = from; i < to; i++) threadSum += array[i];
        threadSums[number] = threadSum;
    }
}
