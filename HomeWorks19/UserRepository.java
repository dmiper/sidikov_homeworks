package HomeWorks;

import java.util.List;

public interface UserRepository {
    List<User> findByAge(int age);
    List<User> findByIsWorkerIsTrue();
}
