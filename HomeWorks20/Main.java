package HomeWorks20;

import static java.lang.System.*;

public class Main {
    public static void main(String[] args) {
        Repository repository = new Repository();

        // Ищем номер автомобиля с оранжевым цветом или с пробегом 0 км
        out.println("1 УСЛОВИЕ :");
        repository.inputCar().stream()
                .filter(cars -> cars.getColor().equalsIgnoreCase("Orange") || cars.getKm() == 0)
                .forEach(cars -> out.println("Автомобиль с оранжевым цветом или с пробегом 0 км - " + cars.getNumber()));

        // Ищем автомобиль ценой между 2 и 3 млн
        out.println("\n" + "2 УСЛОВИЕ :");
        out.println("Вот столько уникальных моделей в ценовом диапазоне от 2 до 3 млн - " + repository.inputCar().stream()
                .filter(cars -> cars.getPrice() >= 2000000 && cars.getPrice() <= 3000000)
                .map(Cars::getNumber)
                .distinct()
                .count());

        // Считаем среднюю цену на Camry
        out.println("\n" + "3 УСЛОВИЕ :");
        out.println("Средняя цена на Camry - " + repository.inputCar().stream()
                .filter(cars -> cars.getModel().equalsIgnoreCase("Camry"))
                .mapToInt(Cars::getPrice)
                .summaryStatistics()
                .getAverage() + " деняг");
    }
}
