package HomeWorks20;

public record Cars(String number, String model, String color, int km, int price) {

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getKm() {
        return km;
    }

    public int getPrice() {
        return price;
    }
}
