package HomeWorks20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Repository {
    public List<Cars> inputCar() {
        List<Cars> cars = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("Car.txt"))) {
            String string;
            while ((string = reader.readLine()) != null) {
                String[] parts = string.split("\\|");

                String number = parts[0];
                String model = parts[1];
                String color = parts[2];
                int km = Integer.parseInt(parts[3]);
                int price = Integer.parseInt(parts[4]);

                cars.add(new Cars(number, model, color, km, price));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return cars;
    }
}
