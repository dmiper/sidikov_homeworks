public class Task07 {
    public static void main(String[] args) {
        int[] arr = new int[500]; //Массив из 500 цифр
        for (int i = 0; i < arr.length; i++) {
            int numberRandom = (int) (Math.random() * 201) - 100; //Рандомные числа для массива
            if (numberRandom != -1){
                arr[i] = numberRandom; //Забиваем рандомными числами массив
            }
        }
        arr[arr.length -1] = -1; //Индекс последнего числа
        System.out.println(numberEnteredAtLeastOnce(arr));
    }

    //Ищет минимальньное количество встреч чисел
    private static int numberEnteredAtLeastOnce(int [] arr) {
        int maxInt = Integer.MAX_VALUE; //Максимальное значение int 2,147,483,647
        int number = 0; //Запоминание числа из массива
        int minimumMeetings = 0; //Запоминает встречи чисел
        int minIndexCount = 0; //Запомимнает индекс числа
        for (int i = 0; arr[i] != -1; i++) {
            number = arr[i]; //Перенос числа из массива
            for (int j = 0; j < arr[j]; j++) {
                if (arr[j] == number) minIndexCount++; //Записывает индекс числа
            }
            if (maxInt > minIndexCount){
                maxInt = minIndexCount;
                minimumMeetings = number; //Проверят и записывает минимальное количество встреч числа
            }
            minIndexCount = 0; //Обнуляет индекс
        }
        return minimumMeetings; //Возвращает значение
    }
}
