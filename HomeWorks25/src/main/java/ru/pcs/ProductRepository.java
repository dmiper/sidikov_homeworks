package ru.pcs;

import java.util.List;

public interface ProductRepository {
    List<Product> findAll();
    List<Product> findAllByPrice(double price);
}
