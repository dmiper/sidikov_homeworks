/* Реализовать ProductsRepository

- List<Product> findAll();
- List<Product> findAllByPrice(double price); - найти товары определенной цены
* List<Product> findAllByOrdersCount(int ordersCount); - найти все товары по количеству заказов, в которых участвуют

*/

package ru.pcs;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_2", "postgres", "0000");
        ProductRepository productRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);
        //Вывод всего товара
        System.out.println(productRepository.findAll());
        // Вывод товара определенной ценовой категории
        System.out.println(productRepository.findAllByPrice(700));

    }
}
