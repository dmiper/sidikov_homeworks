import java.util.*;

public class Task06 {
    public static void main(String[] args) {
        int[] a = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println("Вывод массива: " + Arrays.toString(a));
        System.out.println("Вывод индекса от числа: " + returnIndex(a, 15));
        selectionSort(a);
        System.out.println("Перемещение значемых элементов влево: "Arrays.toString(a));
    }
    
    public static void selectionSort(int[] arr) {
        int minimum = 0;
        for (int s = 0; s < arr.length; s++) {
            if (arr[s] != 0){
                arr[minimum] = arr[s];
                minimum++;
            }
        }
        while (arr.length > minimum){
            arr[minimum++] = 0;
        }
    }

    public static int returnIndex(int[] arr, int num) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == num) {
                return i;
            }
        }
        return index;
    }
}