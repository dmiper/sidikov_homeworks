package ru.pcs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.models.Account;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Optional<Account> findByEmail(String email);
}
