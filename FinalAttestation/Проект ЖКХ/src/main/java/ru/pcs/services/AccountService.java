package ru.pcs.services;

import ru.pcs.forms.AccountForm;

public interface AccountService {
    void signUpAccounts(AccountForm form);
}
