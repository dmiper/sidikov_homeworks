package ru.pcs.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.forms.RecordingsForm;
import ru.pcs.models.Account;
import ru.pcs.models.Recordings;
import ru.pcs.repositories.AccountRepository;
import ru.pcs.repositories.RecordingsRepository;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Component
public class RecordingsServiceImpl implements RecordingsService {

    private final RecordingsRepository recordingsRepository;
    private final AccountRepository accountRepository;

    @Override
    public void addRecording(Integer id_account, RecordingsForm form) {
        Account account = accountRepository.getById(id_account);
        Recordings recordings = Recordings.builder()
                .dateRecordings(LocalDate.parse(form.getDateRecordings()))
                .idNameUtilities(form.getIdNameUtilities())
                .idPaymentAndRecording(form.getIdPaymentAndRecording())
                .record(form.getRecord())
                .owner(account).build();
        recordingsRepository.save(recordings);
    }

    @Override
    public List<Recordings> getAllRecordings() {
        return recordingsRepository.findAll();
    }

    @Override
    public Recordings getRecordings(Integer id) {
        return recordingsRepository.getById(id);
    }

    @Override
    public void deletePatient(Integer id) {
        recordingsRepository.deleteById(id);
    }

    @Override
    public void updatePatient(Integer id, RecordingsForm form) {
        Recordings oldRecording = recordingsRepository.getById(id);
        oldRecording.setDateRecordings(LocalDate.parse(form.getDateRecordings()));
        oldRecording.setIdNameUtilities(form.getIdNameUtilities());
        oldRecording.setIdPaymentAndRecording(form.getIdPaymentAndRecording());
        oldRecording.setRecord(form.getRecord());
        recordingsRepository.save(oldRecording);
    }
}
