package ru.pcs.services;

import ru.pcs.forms.RecordingsForm;
import ru.pcs.models.Recordings;

import java.util.List;

public interface RecordingsService {
    void addRecording(Integer id, RecordingsForm form);

    List<Recordings> getAllRecordings();

    Recordings getRecordings(Integer id);

    void deletePatient(Integer id);

    void updatePatient(Integer id, RecordingsForm form);
}
