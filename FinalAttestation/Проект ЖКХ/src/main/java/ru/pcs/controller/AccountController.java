package ru.pcs.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.forms.AccountForm;
import ru.pcs.services.AccountService;

import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
public class AccountController {
    private final AccountService accountService;

    @GetMapping("/signIn")
    public String getSignInAccounts() {
        return "signIn";
    }

    @GetMapping("/signUp")
    public String getSignUpAccounts() {
        return "signUp";
    }

    @PostMapping("/signUp")
    public String signUpAccounts(@Valid AccountForm form) {
        accountService.signUpAccounts(form);
        return "redirect:/signIn";
    }
}
