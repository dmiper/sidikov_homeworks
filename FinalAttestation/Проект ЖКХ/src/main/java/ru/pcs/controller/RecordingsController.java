package ru.pcs.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.forms.RecordingsForm;
import ru.pcs.models.Recordings;
import ru.pcs.services.RecordingsService;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/recordJkh")
@Controller
public class RecordingsController {
    private final RecordingsService recordingsService;
    final String redirect = "redirect:/recordJkh/";
    final String idRecord = "{idRecording}";

    @PostMapping
    public String addRecording(@AuthenticationPrincipal(expression = "id_account") Integer id_account, @Valid RecordingsForm form) {
        recordingsService.addRecording(id_account, form);
        return redirect;
    }

    @GetMapping
    public String getRecordingsPage(Model model) {
        List<Recordings> recordings = recordingsService.getAllRecordings();
        model.addAttribute("recordJkh", recordings);
        return "recordJkh";
    }

    @GetMapping("/{idRecording}")
    public String getRecordingsPage(Model model, @PathVariable("idRecording") Integer id) {
        Recordings recordings = recordingsService.getRecordings(id);
        model.addAttribute("recording", recordings);
        return "recording";
    }

    @PostMapping("/{idRecording}/delete")
    public String delete(@PathVariable("idRecording") Integer id) {
        recordingsService.deletePatient(id);
        return redirect;
    }

    @PostMapping("/{idRecording}/update")
    public String update(@PathVariable("idRecording") Integer id, @Valid RecordingsForm form) {
        recordingsService.updatePatient(id, form);
        return redirect + idRecord;
    }
}
