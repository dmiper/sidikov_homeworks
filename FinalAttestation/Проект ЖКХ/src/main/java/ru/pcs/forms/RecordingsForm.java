package ru.pcs.forms;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RecordingsForm {
    private Integer id;
    @JsonFormat(pattern = "YYYY-MM-DD")
    private String dateRecordings;
    private Integer idNameUtilities;
    private Integer idPaymentAndRecording;
    @NotNull(message = "Пожалуйста, введите данные")
    private Float record;
}
