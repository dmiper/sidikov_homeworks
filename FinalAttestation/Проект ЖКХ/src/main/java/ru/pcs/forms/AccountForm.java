package ru.pcs.forms;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class AccountForm {
    @NotEmpty(message = "Пожалуйста, введите логин!")
    @Size(min = 2, max = 20, message = "Фамилия должна содержать от 2 до 20 символов!")
    private String loginName;
    @NotEmpty(message = "Пожалуйста, введите почту!")
    @Size(min = 2, max = 20, message = "Почта должна содержать от 8 до 20 символов!")
    private String email;
    @NotEmpty(message = "Пожалуйста, введите пароль!")
    @Size(min = 2, max = 20, message = "Фамилия должна содержать от 8 до 20 символов!")
    private String hashPassword;
    @NotEmpty(message = "Пожалуйста, введите Фамилию Имя Очество!")
    @Size(min = 2, max = 20, message = "Фамилия должна содержать от 2 до 50 символов!")
    private String fullName;
}
