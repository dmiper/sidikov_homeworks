package ru.pcs.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "recordings", schema = "schema")
public class Recordings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDate dateRecordings;
    private Integer idNameUtilities;
    private Integer idPaymentAndRecording;
    @Column
    private Float record;
    @ManyToOne
    @JoinColumn
    private Account owner;
}
