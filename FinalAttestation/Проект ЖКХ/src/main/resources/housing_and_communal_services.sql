create table schema.account
(
    id_account serial primary key, -- id персональных данных
    role varchar(255) not null,
    login_name varchar(55) not null, -- логин
    email varchar(55) not null, -- почта
    hash_password varchar(255) not null, -- пароль
    full_name varchar(55) not null -- ФИО
);

create table schema.payment_and_recording
(
    id integer primary key,
    name varchar
);

insert into schema.payment_and_recording(id, name)
values (1, 'Оплата'),
       (2, 'Запись');

create table schema.utilities
(
    id integer primary key,
    service_name varchar -- имя услуги
);

insert into schema.utilities(id, service_name)
values  (0, 'Нет'),
        (1, 'Газ'),
        (2, 'Электричество'),
        (3, 'Холодная вода'),
        (4, 'Горячая вода'),
        (5, 'Отопление'),
        (6, 'Капремонт');

create table schema.recordings
(
    id serial primary key,
    date_recordings date, -- дата
    id_name_utilities integer, -- id поставки услуги
    id_payment_and_recording integer, --id оплаты или запись
    record dec not null, -- показания

    foreign key (id_name_utilities) references schema.utilities(id),
    foreign key (id_payment_and_recording) references schema.payment_and_recording(id)
);
