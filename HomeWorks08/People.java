record People(String name, double weight) implements Comparable<People> {

    public double getWeight() {
        return weight;
    }

    // Выведет данные в стоку
    public String toString() {
        return "Человек: " + "имя " + name + ", вес " + weight + " килограмм.";
    }

    // Сравнивает объекты
    public int compareTo(People o) {
        int result = 0;
        if (this.getWeight() < o.getWeight()) {
            result = -1;
        } else if (this.getWeight() > o.getWeight()) {
            result = 1;
        }
        return result;
    }
}
