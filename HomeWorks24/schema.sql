create table product
(
    id serial primary key,
    description varchar(100),
    cost_RUB integer check ( cost_RUB > 0 ),
    quantity integer check ( quantity > 0 )
);
-- Заполняем таблицу Product
insert into product(description, cost_RUB, quantity)values ('Шоколадное печенье', 49, 3000);
insert into product(description, cost_RUB, quantity)values ('Торт Наполеон', 799, 100);
insert into product(description, cost_RUB, quantity)values ('Овсяное печенье', 73, 1000);
insert into product(description, cost_RUB, quantity)values ('Клубничная пастила', 120, 1200);
insert into product(description, cost_RUB, quantity)values ('Ультрапастеризованное молоко', 150, 1000);
insert into product(description, cost_RUB, quantity)values ('Сливки 30%', 320, 900);
insert into product(description, cost_RUB, quantity)values ('Йогурт белый', 90, 500);
insert into product(description, cost_RUB, quantity)values ('Хлебцы ', 75, 2300);
insert into product(description, cost_RUB, quantity)values ('Хлеб из злаков', 69, 1200);
insert into product(description, cost_RUB, quantity)values ('Доширак из курицы', 39, 5000);
insert into product(description, cost_RUB, quantity)values ('Пельмени из говядины', 689, 2000);
insert into product(description, cost_RUB, quantity)values ('Криветки королевские 1кг', 999, 50);
insert into product(description, cost_RUB, quantity)values ('Чай зеленый листовой', 450, 3500);
insert into product(description, cost_RUB, quantity)values ('Чай черный пакетированный', 325, 2500);
insert into product(description, cost_RUB, quantity)values ('Кофе в зернах "Арабика"', 1499, 500);
insert into product(description, cost_RUB, quantity)values ('Сыр Брынца', 220, 1700);
insert into product(description, cost_RUB, quantity)values ('Помидоры 1кг', 190, 45);
insert into product(description, cost_RUB, quantity)values ('Огурцы пупырчатые 1кг', 210, 50);
insert into product(description, cost_RUB, quantity)values ('Ананас 1шт', 350, 50);
insert into product(description, cost_RUB, quantity)values ('Вино красное', 1400, 200);
insert into product(description, cost_RUB, quantity)values ('Вино белое', 1500, 200);
insert into product(description, cost_RUB, quantity) values ('Пиво безалкогольное', 120, 2000);
insert into product(description, cost_RUB, quantity) values ('Энергетик', 99, 3500);
insert into product(description, cost_RUB, quantity) values ('Детское шампанское', 250, 3500);
insert into product(description, cost_RUB, quantity) values ('Газировка', 170, 5000);


create table customer
(
    id serial primary key,
    first_and_last_name varchar(20)
);
-- Заполняем таблицу Customer
insert into customer(first_and_last_name) values ('Прохор Еримин');
insert into customer(first_and_last_name) values ('Эмили Зуева');
insert into customer(first_and_last_name) values ('Урсула Львова');
insert into customer(first_and_last_name) values ('Максим Шмелов');
insert into customer(first_and_last_name) values ('Виссарион Золотов');
insert into customer(first_and_last_name) values ('Оскар Шапошников');
insert into customer(first_and_last_name) values ('Янина Успенская');
insert into customer(first_and_last_name) values ('Мирра Демьянова');
insert into customer(first_and_last_name) values ('Ждан Беликов');
insert into customer(first_and_last_name) values ('Грейс Косарев');
insert into customer(first_and_last_name) values ('Доминика Козина');
insert into customer(first_and_last_name) values ('Тимофей Ежов');
insert into customer(first_and_last_name) values ('Фаина Коршунова');
insert into customer(first_and_last_name) values ('Аристарх Гущин');
insert into customer(first_and_last_name) values ('Назар Кочергин');
insert into customer(first_and_last_name) values ('Юлиан Сурков');
insert into customer(first_and_last_name) values ('Аполлинарий Агапов');
insert into customer(first_and_last_name) values ('Ефросинья Чеботаева');
insert into customer(first_and_last_name) values ('Рику Сомов');
insert into customer(first_and_last_name) values ('Ия Ракова');
insert into customer(first_and_last_name) values ('Петр Поликарпов');
insert into customer(first_and_last_name) values ('Серафима Гладкова');
insert into customer(first_and_last_name) values ('Зоряна Судакова');
insert into customer(first_and_last_name) values ('Филат Елизаров');


create table order_customer
(
    id serial primary key,
    product_id integer,
    customer_id integer,
    order_date varchar(10),
    number_of_products integer check ( number_of_products > 0 ),
    foreign key(product_id) references product(id),
    foreign key(customer_id) references customer(id)
);
-- Заполняем таблицу Order Customer
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (25, 4, '31.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (23, 4, '31.12.2021', '10');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (18, 4, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (19, 4, '31.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (2, 4, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (8, 4, '31.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 8, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (20, 8, '31.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (12, 8, '31.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (2, 8, '31.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (13, 8, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (17, 1, '31.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (18, 1, '31.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (16, 1, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (1, 1, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (6, 1, '31.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (25, 1, '31.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (19, 14, '31.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (22, 14, '31.12.2021', '10');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (10, 14, '31.12.2021', '10');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (11, 14, '31.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (24, 11, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 11, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (12, 11, '31.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (9, 11, '31.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (7, 11, '31.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (20, 12, '31.12.2021', '6');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (12, 12, '31.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (15, 24, '30.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (5, 24, '30.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (6, 24, '30.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (7, 24, '30.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (8, 24, '30.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (3, 22, '30.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (9, 22, '30.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (7, 22, '30.12.2021', '5');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (15, 22, '30.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (10, 21, '30.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (23, 21, '30.12.2021', '6');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (20, 19, '30.12.2021', '10');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 19, '30.12.2021', '10');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (12, 19, '30.12.2021', '25');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (25, 10, '30.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (14, 10, '30.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (3, 10, '30.12.2021', '5');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (9, 10, '30.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (18, 2, '30.12.2021', '6');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (17, 2, '30.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (23, 9, '30.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (18, 9, '29.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (16, 9, '29.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 2, '29.12.2021', '9');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 2, '29.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (18, 12, '29.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (25, 12, '29.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 12, '29.12.2021', '7');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (1, 12, '29.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (18, 16, '29.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (17, 16, '29.12.2021', '5');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (14, 16, '29.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (12, 16, '29.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (10, 16, '29.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (1, 23, '29.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (10, 23, '29.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (20, 23, '29.12.2021', '5');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 23, '29.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 5, '28.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (12, 5, '28.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (22, 5, '28.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (11, 5, '28.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (2, 5, '28.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (1, 5, '28.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (1, 17, '28.12.2021', '5');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (25, 17, '28.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (5, 17, '28.12.2021', '6');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (2, 17, '28.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (24, 20, '28.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (4, 20, '28.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (2, 20, '28.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (7, 3, '27.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (9, 3, '27.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (11, 3, '27.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (13, 3, '27.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (15, 3, '27.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (25, 13, '27.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (23, 13, '27.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 13, '27.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (1, 13, '27.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (3, 13, '27.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (13, 13, '27.12.2021', '5');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (11, 13, '27.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 13, '27.12.2021', '5');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (13, 6, '27.12.2021', '10');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (2, 6, '27.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (3, 6, '27.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (1, 6, '27.12.2021', '8');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (24, 6, '27.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (23, 18, '26.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (15, 18, '26.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (2, 18, '26.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (4, 7, '26.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (18, 7, '26.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (17, 7, '26.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (16, 7, '26.12.2021', '1');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (12, 7, '26.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (15, 15, '26.12.2021', '2');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (20, 15, '26.12.2021', '3');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (21, 15, '26.12.2021', '5');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (7, 15, '26.12.2021', '7');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (3, 15, '26.12.2021', '9');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (8, 15, '26.12.2021', '6');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (4, 15, '26.12.2021', '4');
insert into order_customer(product_id, customer_id, order_date, number_of_products)values (1, 15, '26.12.2021', '2');


-- Получить товары и число проданного товара
select description as product_description,
       (select sum(number_of_products)
        from order_customer
        where product.id = order_customer.product_id) as total_quantity
from product
order by total_quantity;

-- Получить заказчиков и число заказов, общее количество заказаных товаров и общую стоимость
select first_and_last_name as customer,
       (select count(*)
        from order_customer
        where customer_id = customer.id) as order_count,
       (select sum(number_of_products)
        from order_customer
        where customer_id = customer.id) as total_amout,
       (select sum(cost_RUB * order_customer.number_of_products)
        from product, order_customer where customer.id = order_customer.customer_id)
from customer;

-- Объединить обе таблицы
select * from order_customer g_order
                  full join product p on g_order.product_id = p.id
                  full join customer c on c.id = g_order.customer_id
