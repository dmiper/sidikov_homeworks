public class Ellipse extends Figure {

    public Ellipse(double r1, double r2, double x, double y, String name) {
        super(r1, r2, x, y, name);
    }

    @Override
    public double getPerimeter() {
        return 4 * ((3.14 * getR1() * getR2() + Math.pow((getR1() - getR2()), 2)) / (getR1() + getR1()));
    }
}
