public class Main {

    public static void main(String[] args) {
        Portable[] figures = new Portable[] {
                new Square(20, 10, 5, 15, "квадрат"),
                new Circle(6, 3, 2, 5, "круг")
        };
        moveShape(figures, -9, 9);
        showCoordinates(figures);
    }

    private static void showCoordinates(Portable[] figures) {
        for (Portable figure : figures)
            System.out.println("Центр координат фигуры " +
                    ((Figure) figure).getName() +
                    ": " +
                    ((Figure) figure).getX() +
                    ", " +
                    ((Figure) figure).getY());
    }

    public static void moveShape(Portable[] figures, double newX, double newY) {

        for (Portable figure : figures) {
            figure.moveShape(newX, newY);
        }
    }
}
