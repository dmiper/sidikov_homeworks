package HomeWorks;

public class ArrayList<T> {
    private static final int DEFAULT_SIZE = 10;

    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    /**
     * Добавляет элемент в конец списка
     * @param element добавляемый элемент
     */
    public void add(T element) {
        // если массив уже заполнен
        if (isFullArray()) {
            resize();
        }

        this.elements[size] = element;
        size++;
    }

    private void resize() {
        // запоминаем старый массив
        T[] oldElements = this.elements;
        // создаем новый массив, который в полтора раза больше старого
        this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
        // копируем из старого массива в новый
        System.arraycopy(oldElements, 0, this.elements, 0, size);
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * Получить элемент по индексу
     * @param index индекс искомого элемента
     * @return элемент под заданным индексом
     */
    public T get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * удаление элемента по индексу
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index){
        elements[index] = null;
        for (int i = (index + 1); i < elements.length - 1; i++) {
            elements[i - 1] = elements[i];
            elements[i] = null;
        }
        size--;
    }
}
