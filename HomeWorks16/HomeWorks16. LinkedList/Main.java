package List;

public class Main {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        massive(list);

        System.out.println(list.get(0));
        System.out.println(list.get(3));
        System.out.println(list.get(6));
    }

    private static void massive(LinkedList<Integer> list) {
        list.add(40);
        list.add(50);
        list.addToBegin(30);
        list.add(60);
        list.addToBegin(20);
        list.add(70);
        list.addToBegin(10);
    }
}
